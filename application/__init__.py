# app/__init__.py

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def init_app():
    # initialize the app
    app = Flask(
        __name__,
        instance_relative_config=True,
        static_url_path='/static'
    )
    app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
    
    # load the config file
    app.config.from_object('config.DevConfig')

    db.init_app(app)

    with app.app_context():
        # load the views/routes
        from . import views
        db.create_all()

        return app
