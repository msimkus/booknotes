# app/views.py

from flask import request, render_template, redirect, url_for, flash
from flask import current_app as app
from application.models import db, Book


@app.route('/')
def index():
    books = db.session.query(Book).all()
    books = enumerate(books, 1)

    return render_template('index.html', books=books)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'GET':
        return render_template('create.html')

    if request.method == 'POST':
        if 'submit-btn' in request.form:
            # create a new book object
            author = request.form['author']
            title = request.form['title']
            notes = request.form['notes']
            new_book = Book(
                author=author,
                title=title,
                notes=notes
            )
            
            # update the database
            db.session.add(new_book)
            db.session.commit()
            
            flash(u'BookNote was successfully created.', 'success')

    return redirect(url_for('index'))

@app.route('/details/<int:book_id>', methods=['GET', 'POST'])
def details(book_id):
    if book_id is not None:
        book_to_view = db.session.query(Book).get_or_404(book_id)
    if book_to_view is not None:
        if request.method == 'GET':
            return render_template('details.html', book=book_to_view)
        if request.method == 'POST':
            if 'cancel-btn' in request.form:
                return redirect(url_for('index'))
            elif 'edit-btn' in request.form:
                return render_template('update.html', book=book_to_view)
            elif 'delete-btn' in request.form:
                return render_template('delete.html', book=book_to_view)
    else:
        flash(u'BookNote could not be found.', 'error')

    return redirect(url_for('index'))

@app.route('/update/<int:book_id>', methods=['GET', 'POST'])
def update(book_id):
    if book_id is not None:
        book_to_update = db.session.query(Book).get_or_404(book_id)
    if book_to_update is not None:
        if request.method == 'GET':
            return render_template('update.html', book=book_to_update)
        if request.method == 'POST':
            if 'submit-btn' in request.form:
                # update the book object
                book_to_update.author = request.form['author']
                book_to_update.title = request.form['title']
                book_to_update.notes = request.form['notes']
                
                # update the database
                db.session.commit()
                
                flash(u'BookNote info was successfully updated.', 'success')
    else:
        flash(u'BookNote could not be found.', 'error')

    return redirect(url_for('index'))

@app.route('/delete/<int:book_id>', methods=['GET', 'POST'])
def delete(book_id):
    if book_id is not None:
        book_to_delete = db.session.query(Book).get_or_404(book_id)
    if book_to_delete is not None:
        if request.method == 'GET':
            return render_template('delete.html', book=book_to_delete)
        if request.method == 'POST':
            if 'submit-btn' in request.form:
                # update the database
                db.session.delete(book_to_delete)
                db.session.commit()

                flash(u'BookNote was successfully deleted.', 'success')
    else:
        flash(u'BookNote could not be found.', 'error')
        
    return redirect(url_for('index'))
