# app/models.py

from application import db


class Book(db.Model):
    """Data model for books"""

    __tablename__ = 'book'

    id = db.Column(
        db.Integer(),
        primary_key=True
    )
    author = db.Column(
        db.String()
    )
    title = db.Column(
        db.String()
    )
    notes = db.Column(
        db.Text()
    )

    def __init__(self, author, title, notes):
        self.author = author
        self.title = title
        self.notes = notes
    