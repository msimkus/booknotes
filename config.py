# config.py

class DevConfig:
    """Configuration for development mode"""
    
    # general configuration
    DEBUG = True
    
    # database configuration
    SQLALCHEMY_DATABASE_URI = 'sqlite:///booknotes.db'
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProdConfig:
    """Configuration for production mode"""
    
    # general configuration
    DEBUG = False
